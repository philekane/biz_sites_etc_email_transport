<?php
/**
 * Basic WordPress Plugin SMTP Mailer
 *
 * PHP version 7.2
 *
 * @category Email_Transport
 * @package  Biz_Sites_Etc_Email_Transport
 * @author   Phil Kane <pkane-pluginDev@spindry.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  1.0
 * @link     https://business-websites-etc.com *
 */
/*
Plugin Name:  Biz Sites Etc - Email Transport
Plugin URI:   https://plugins.business-websites-etc.com/biz-sites-etc_email_transport
Description:  Basic WordPress Plugin Email Transport
Version:      1.0
Author:       Phil Kane, Business Websites, Etcetera
Author URI:   https://business-websites-etc.com/about
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages

biz-sites-etc_email_transport is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

biz-sites-etc_email_transport is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with biz-sites-etc_email-transport. If not, see {URI to Plugin License}.
 */
if (!defined('ABSPATH')) {
    die('No direct access allowed');
}

if (!class_exists('Biz_Sites_Etc_Email_Transport')) {
    /**
     * A Biz_Sites_Etc_Email_Transport class configures SMTP credentials in order
     * to use a third-party server to send email using wp_mail
     *
     * @category Email_Transport
     * @package  Category
     * @author   Phil Kane <pkane-pluginDev@spindry.com>
     * @license  MIT https://opensource.org/licenses/MIT
     * @link     https://business-websites-etc.com/
     */
    class Biz_Sites_Etc_Email_Transport
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // Initialize Settings
            include_once sprintf("%s/settings.php", dirname(__FILE__));
            $Biz_Sites_Etc_Email_Transport_Settings = new Biz_Sites_Etc_Email_Transport_Settings();
            $plugin = plugin_basename(__FILE__);
            add_filter("plugin_action_links_$plugin", array($this, 'pluginSettingsLink'));
        } // END public function __construct
        /**
         * Activate the plugin
         * 
         * @return null
         */
        public static function activate()
        {
            // Do nothing
        } // END public static function activate
        /**
         * Deactivate the plugin
         * 
         * @return null
         */
        public static function deactivate()
        {
            // Delete smtp fields
           // delete_option('smtpserver');
           // delete_option('smtpport');
           // delete_option('smtpusername');
           // delete_option('smtppwd');
            
        } // END public static function deactivate

        /**
         * Add the settings link to the plugins page
         *
         * @param mixed $links 
         * 
         * @return $links
         */
        public function pluginSettingsLink($links)
        {
            $settings_link = '<a href="options-general.php?page=biz-sites-etc-email-transport">Settings</a>';
            array_unshift($links, $settings_link);
            return $links;
        }
    } // END class Biz_Sites_Etc_Email_Transport
} // END if(!class_exists('Biz_Sites_Etc_Email_Transport'))
if (class_exists('Biz_Sites_Etc_Email_Transport')) {
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('Biz_Sites_Etc_Email_Transport', 'activate'));
    register_deactivation_hook(__FILE__, array('Biz_Sites_Etc_Email_Transport', 'deactivate'));
    // instantiate the plugin class
    $biz_sites_etc_email_transport = new Biz_Sites_Etc_Email_Transport();
}
