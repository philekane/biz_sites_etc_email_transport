=== SMTP Email Transport ===
Contributors: Phil Kane
Tags: smtp, phpmailer, wp_mail()
Requires at least: 4.9.5
Tested up to: 4.9.6
Requires PHP: 5.6

Use any custom SMTP (e.g. google, sendGrid, rackSpace to send email

== Description ==
Use any SMTP  to send an email 

== Installation ==
Install

== Frequently Asked Questions ==
FAQ

== Screenshots ==
1. SMTP settings on the admin page

