<?php
/**
 * Biz_Sites_Etc_Email_Transport class configures SMTP credentials in order
 * to use a third-party server to send email using wp_mail
 *
 * @category Email_Transport
 * @package  Category
 * @author   Phil Kane <pkane-pluginDev@spindry.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://business-websites-etc.com/
 */
if (!class_exists('Biz_Sites_Etc_Email_Transport_Settings')) {
    /**
     * Set custom credentials for Email Transport
     *
     * @category Email_Transport
     * @package  Category
     * @author   Phil Kane <pkane-pluginDev@spindry.com>
     * @license  MIT https://opensource.org/licenses/MIT
     * @link     https://business-websites-etc.com/
     */
    class Biz_Sites_Etc_Email_Transport_Settings
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // register actions
            add_action('admin_init', array(&$this, 'adminInit'));
            add_action('admin_menu', array(&$this, 'addMenu'));
            add_action('phpmailer_init', array(&$this, 'mailerConfig'));
        } // END public function __construct
       
        /**
         * MailerConfig changes wp_mail() to use custom smtp credentials
         * 
         * @param mixed $phpmailer changes wp_mail to custom smtp credentials
         *
         * @return null
         */
        public function mailerConfig($phpmailer)
        {
            if (!is_object($phpmailer)) {
                $phpmailer = (object) $phpmailer;
            }
            $phpmailer->Mailer      = 'smtp';
            $phpmailer->Host        = get_option('smtpserver');
            $phpmailer->SMTPAuth    = true;
            $phpmailer->Port        = get_option('smtpport');
            $phpmailer->Username    = get_option('smtpusername');
            $phpmailer->Password    = get_option('smtppwd');
            $phpmailer->SMTPSecure  = 'tls';
            $phpmailer->From        = 'phil.kane@business-websites-etc.com';
            $phpmailer->FromName    = get_bloginfo('name');
            /* Fixing  PHP Warning:  stream_socket_enable_crypto():
             * SSL operation failed
             * https://gist.github.com/unclego/f7682de5820a1c76d473
             */
            $phpmailer->SMTPOptions = array('ssl' => array( 'verify_peer' => false,
             'verify_peer_name' => false, 'allow_self_signed' => true ));
           
        }
        /**
         * Hook into WP's admin_init action hook
         * 
         * @return mixed
         */
        public function adminInit()
        {
            // register your plugin's settings
            register_setting('biz_sites_etc_email_transport-group', 'smtpserver');
            register_setting('biz_sites_etc_email_transport-group', 'smtpport');
            register_setting('biz_sites_etc_email_transport-group', 'smtpusername');
            register_setting('biz_sites_etc_email_transport-group', 'smtppwd');
            register_setting('biz_sites_etc_email_transport-group', 'smtpdestinationemail');

            // add your settings section
            add_settings_section(
                'biz_sites_etc_email_transport-section',
                'Email Transport - SMTP Setting',
                array(&$this, 'settingsSectionBizSitesEtcEmailTransport'),
                'biz_sites_etc_email_transport'
            );

            // add your setting's fields
            add_settings_field(
                'biz_sites_etc_email_transport-smtpserver',
                'SMTP Server ( e.g., smtp.gmail.com)',
                array(&$this, 'settingsFieldInputText'),
                'biz_sites_etc_email_transport',
                'biz_sites_etc_email_transport-section',
                array(
                    'field' => 'smtpserver',
                    'default' => 'smtp.gmail.com',
                    'type' => 'text',
                )
            );
            add_settings_field(
                'biz_sites_etc_email_transport-smtpport',
                'SMTP Port (googles - e.g., 465)',
                array(&$this, 'settingsFieldInputText'),
                'biz_sites_etc_email_transport',
                'biz_sites_etc_email_transport-section',
                array(
                    'field' => 'smtpport',
                    'default' => '',
                    'type' => 'number',
                )
            );
            add_settings_field(
                'biz_sites_etc_email_transport-smtpusername',
                'Username ( e.g., example@gmail.com or apikey)',
                array(&$this, 'settingsFieldInputText'),
                'biz_sites_etc_email_transport',
                'biz_sites_etc_email_transport-section',
                array(
                    'field' => 'smtpusername',
                    'default' => '',
                    'type' => 'text',
                )
            );

            add_settings_field(
                'biz_sites_etc_email_transport-smtpdestinationemail',
                'Destination Email (if other than admin email)',
                array(&$this, 'settingsFieldInputText'),
                'biz_sites_etc_email_transport',
                'biz_sites_etc_email_transport-section',
                array(
                    'field' => 'smtpdestinationemail',
                    'default' => '',
                    'type' => 'email',
                )
            );
            add_settings_field(
                'biz_sites_etc_email_transport-smtppwd',
                'Password ',
                array(&$this, 'settingsFieldInputText'),
                'biz_sites_etc_email_transport',
                'biz_sites_etc_email_transport-section',
                array(
                    'field' => 'smtppwd',
                    'default' => '',
                    'type' => 'text',
                )
            );
            // Possibly do additional admin_init tasks
        } // END public static function activate
        /**
         * Help text for section
         * 
         * @return mixed
         */
        public function settingsSectionBizSitesEtcEmailTransport()
        {
            // Think of this as help text for the section.
            echo 'Add credentials for a SMTP server, and destination email if different than admin_email, for sending email. All transports use TLS. ';
        }

        /**
         * This function provides text inputs for settings fields
         * 
         * @param mixed $args creating array for input fields
         * 
         * @return mixed input fields
         */
        public function settingsFieldInputText($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);

            // echo a proper input type="text"
            if ($field == 'smtpdestinationemail' ) {
                echo sprintf('<input type="%s" name="%s" id="%s" value="%s"  />', $type, $field, $field, $value, $default);
            } else {
                echo sprintf('<input type="%s" name="%s" id="%s" value="%s" required />', $type, $field, $field, $value, $default);
            }

        } // END public function settings_field_input_text($args)

        /**
         * Add a menu
         *
         * @return mixed
         */
        public function addMenu()
        {
            // Add a page to manage this plugin's settings
            add_options_page(
                'Biz Sites, Etc Settings',
                'Biz Sites, Etc - Email Transport Settings',
                'manage_options',
                'biz-sites-etc-email-transport',
                array(&$this, 'pluginSettingsPage')
            );
        } // END public function add_menu()

        /**
         * Menu Callback
         * 
         * @return error message
         */
        public function pluginSettingsPage()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            // Render the settings template
            include sprintf("%s/templates/settings.php", dirname(__FILE__));
        } // END public function plugin_settings_page()
    } // END class Biz_Sites_Etc_Email_Transport_Settings
} // END if(!class_exists('Biz_Sites_Etc_Email_Transport_Settings'))
